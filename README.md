# invex

Invidious URL grabber

invex is a simple CLI tool which lets you search for YouTube videos through 
Invidious. The result gives you a URL which you can use for anything -- you 
can paste the URL to mpv, youtube-dl, or anywhere else you'd like. Gone are 
the days where you have to open up a browser and jump through menus and 
loading times and thumbnails of silly faces, ads, and clickbait.

Inspired by [mpv](https://mpv.io "mpv") and [youtube-dl](https://youtube-dl.org/
"youtube-dl"), invex was created so that both tools could be used without the 
need to open a browser to get the URL in the first place.

### Usage

Run this in the terminal:

    git clone https://gitlab.com/steelinferno/invex.git && cd invex
    ./invex.py

![startup](screenshots/step1.png)
    
Type in what you want to search, After the results load, it'll prompt you to
pick a number. 

![results](screenshots/step2.png)

For the final prompt, here are some sample options:

    >0 # copies the URL of the first video to the clipboard
    >mpv 0 # streams the first video in mpv 
    >vlc 0 # opens the first video in vlc
    >ydl 0 # downloads first the video
    >youtube-dl 0 # downloads the first video

![end](screenshots/step3.png)

You are now ready to easily search and view videos from the command line. Enjoy!

### Known issues

- st (simple terminal) crashes if you search for "best pirate car". tty works 
fine. Needs more testing. 
- fish terminal needs quotes around the URL to open with mpv and such

**[ytdl-hook] youtube-dl failed: unexpected error ocurred**

Try updating youtube-dl. It needs to be regularly updated to work, since it's 
a web scraper and not an api.

**ffmpeg - tcp: Failed to resolve hostname**

Try updating ffmpeg. At this point you should probably update your whole system.

### New in v0.5
- macOS/Windows compatibility
- open with vlc feature
- wayland clipboard support
- more exception handling

### Further plans

- add a man page
- add invex to repos
- animated loading ascii for the mpv option
- remove URL from view as it just takes up space and is copied anyway
- show video length, views, some more information to make the video distinct
- More flexibility, ie. 'youtube-dl -x 3', etc
- url-sanitizing tools
- exception handling for having both 'mpv' and 'youtube-dl' in the final prompt
- use a scraper instead of invidious api for added privacy

Do you like invex? Feel free to star my project and move the script to /bin:

    sudo mv invex.py invex && sudo chmod +x invex && sudo mv invex /bin/

invex is much faster than mps-youtube and it uses Invidious API instead of
YouTube API!
